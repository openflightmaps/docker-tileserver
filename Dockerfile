FROM klokantech/tileserver-gl
RUN apt-get update && apt-get install -y git && rm -rf /var/lib/apt/lists/*
RUN git clone https://github.com/openflightmaps/ofm-hems-gl-style.git /usr/src/app/node_modules/tileserver-gl-styles/styles/ofm-hems-gl-style
RUN git clone https://github.com/newayData/OEAMTC-osm-street-style.git /usr/src/app/node_modules/tileserver-gl-styles/styles/OEAMTC-osm-street-style
